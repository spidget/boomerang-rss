#!/usr/bin/env python3

import csv
import re

from bs4 import BeautifulSoup
from collections import Counter
from xml.dom import minidom
from email.utils import formatdate
from pprint import pprint
from requests import get

import PyRSS2Gen


csv_path = 'public/boomerang.csv'
rss_path = 'public/boomerang.rss'
cols=["title", "platform", "genres", "link", "seen", "page"]


def get_games(page=1, offset=0):
    root = "https://www.boomerangrentals.co.uk/game-catalogue.php?pno="

    running = True
    while running:
        html = get(root + str(page)).text
        soup = BeautifulSoup(html, 'html.parser')
        games = soup.find('h1').parent.find_all('a')

        for game in games[offset:]:
            url = game['href']
            matches = re.search(
                    "Rent (.*) for (.*) - (.*) Game Rentals",
                    game.text)
            yield {
                "title": matches[1],
                "platform": matches[2].lower(),
                "genres": matches[3].lower(),
                "link": url,
                "seen": formatdate(),
                "page": page
            }

        page = page + 1
        offset = 0
        running = len(games) > 0


def make_item(g):
    return PyRSS2Gen.RSSItem(
            title="{} ({})".format(g['title'], g['platform']),
            link=g['link'],
            pubDate=g['seen'],
            guid=PyRSS2Gen.Guid(g['link']),
            description=g['genres'])


def start_from():
    try:
        with open(csv_path) as f:
            pages = [int(g['page']) for g in csv.DictReader(f, fieldnames=cols)]
            page = max(pages, default=1)
            offset = Counter(pages)[page]
            return page, offset
    except FileNotFoundError:
        return 1, 0


def update_csv():
    page, offset = start_from()
    updated = False
    with open(csv_path, 'a') as f:
        w = csv.DictWriter(f, fieldnames=cols)
        for g in get_games(page=page, offset=offset):
            pprint(g)
            w.writerow(g)
            updated = True
    return updated


def create_rss():
    with open(csv_path) as i, open(rss_path, 'w') as o:
        games = [make_item(g) for g in csv.DictReader(i, fieldnames=cols)]

        rss = PyRSS2Gen.RSS2(
                title="Boomerang Rentals games",
                description="Boomerang Rentals games",
                link="https://spidget.gitlab.io/boomerang-rss/boomerang.rss",
                lastBuildDate=formatdate(),
                items=games[-200:])

        rss.write_xml(o)

    xml = minidom.parse(rss_path).toprettyxml(indent="    ")
    with open(rss_path, 'w') as f:
        f.write(xml)


if __name__ == "__main__":
    if update_csv():
        create_rss()
